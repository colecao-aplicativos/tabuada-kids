export const gerarNumero = () => {
    return Math.floor(Math.random() * (10 + 1 - 1) + 1);
}

export const validarResposta = (numero1: number, numero2: number, respostaUsuario: string) => {
    return parseInt(respostaUsuario) ==  (numero1 * numero2);
}
