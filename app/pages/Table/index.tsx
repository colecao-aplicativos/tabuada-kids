import React from 'react';
import {ImageBackground, View, Button, StyleSheet} from 'react-native';

import table from '../../../assets/tabuada.png';

export function Table(props: any) {
    const voltar = () => {
        props.navigation.navigate("Home");
    }
    return (
        <ImageBackground source={table} style={estilo.fundo} resizeMode="contain">
            <View style={estilo.boxBotaoTabuada}>
                <Button title="Responder" onPress={voltar} color="#a0df52"/>
            </View>
        </ImageBackground>
    );
}

const estilo = StyleSheet.create({
    fundo: {
        width: '100%',
        height: "100%",
        backgroundColor: "rgb(255,249,215)",
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: "center"
    },
    boxBotaoTabuada: {
        width: 180,
        marginBottom: 10
    }
});
