import React, {useState} from "react";
import {View, Text, Button, TextInput, ImageBackground, StyleSheet}
    from "react-native";
import {validarResposta, gerarNumero} from '../../services';

import fundo from '../../../assets/icon.png';

export function Home(props: any) {
    const [primeiroNumero, setPrimeiroNumero] = useState(1);
    const [segundoNumero, setSegundoNumero] = useState(1);
    const [respostaUsuario, setRespostaUsuario] = useState("0");

    const criarQuestao = () => {
        setPrimeiroNumero(gerarNumero());
        setSegundoNumero(gerarNumero());
        setRespostaUsuario("");
    }

    const responder = () => {
        if (validarResposta(primeiroNumero, segundoNumero, respostaUsuario)) {
            props.navigation.navigate('RespostaCorreta');
        } else {
            props.navigation.navigate('RespostaErrada');
        }

        criarQuestao();
    }

    const abrirTelaTabuada = () => {
        props.navigation.navigate('Tabuada');
    }

    return (
        <ImageBackground source={fundo} style={estilo.imagemFundo}>
            <View style={estilo.tela}>
                <View style={estilo.boxPergunta}>
                    <Text style={estilo.titulo}>
                        Duvido você acertar!
                    </Text>

                    <View style={estilo.boxQuestao}>
                        <Text style={estilo.texto}>
                            {primeiroNumero}
                        </Text>

                        <Text style={estilo.texto}>
                            X
                        </Text>

                        <Text style={estilo.texto}>
                            {segundoNumero}
                        </Text>

                        <Text style={estilo.texto}>
                            =
                        </Text>

                        <TextInput
                            textAlign="center"
                            onChangeText={setRespostaUsuario}
                            keyboardType="numeric"
                            value={respostaUsuario.toString()}
                            autoFocus={true}
                            maxLength={3}
                            style={estilo.input}/>
                    </View>

                    <View style={estilo.opcoes}>
                        <View style={estilo.boxBotao}>
                            <Button title="Pular" onPress={criarQuestao} color="#e53b62"/>
                        </View>

                        <View style={estilo.boxBotao}>
                            <Button title="Responder" onPress={responder} color="#a0df52"/>
                        </View>
                    </View>

                </View>

                <View style={estilo.boxBotaoTabuada}>
                    <Button title="Ver tabuada"
                            onPress={abrirTelaTabuada} color="#1f4f66"/>
                </View>
            </View>
        </ImageBackground>
    );
}

const estilo = StyleSheet.create({
    imagemFundo: {
        width: '100%',
        height: '100%'
    },
    tela: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    boxPergunta: {
        backgroundColor: 'rgba(255,255,255,0.85)',
        borderRadius: 10,
        margin: 20,
        padding: 20,
        alignItems: "center",
        width: 320
    },
    titulo: {
        marginBottom: 20,
        paddingHorizontal: 10,
        fontSize: 26,
        textTransform: "uppercase",
        fontWeight: "700",
        textAlign: "center",
        color: "#1f4f66"
    },
    boxQuestao: {
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center"
    },
    texto: {
        marginRight: 10,
        fontSize: 26
    },

    input: {
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 3,
        width: 100,
        paddingHorizontal: 5,
        marginLeft: 10,
        fontSize: 26
    },
    opcoes: {
        width: 320,
        marginTop: 20,
        alignItems: "center",
        justifyContent: 'space-around',
        flexDirection: 'row'
    },
    boxBotao: {
        minWidth: 130,
        marginBottom: 10
    },
    boxBotaoTabuada: {
        width: 320
    }
});
