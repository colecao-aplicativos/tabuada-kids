import React from 'react';
import {View, Text, ImageBackground, Button, StyleSheet} from 'react-native';

import trofeu from '../../../assets/fundo.png';

export function CorrectAnswer(props: any) {

  const backButton = () => {
    props.navigation.goBack();
  }

  return (
    <ImageBackground source={trofeu} style={style.fundo} resizeMode="contain">
      <Text style={style.texto}>Parabéns, você acertou!</Text>

      <View style={style.boxBotaoTabuada}>
        <Button title="Responder outro" onPress={backButton} color="#a0df52" />
      </View>
    </ImageBackground>
  );
}

const style = StyleSheet.create({
    fundo: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        flex: 1,
        justifyContent: 'space-around',
        alignItems:"center"
    },

    texto: {
        marginBottom: 80,
        paddingHorizontal: 10,
        fontSize: 26,
        textTransform: "uppercase",
        fontWeight: "700",
        textAlign: "center",
        color:"#1f4f66"
    },
    boxBotaoTabuada: {
        marginTop: 80,
        width: 180
    }
});
