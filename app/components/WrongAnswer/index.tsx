import React from 'react';
import {Text, ImageBackground, View, Button, StyleSheet} from 'react-native';

import errou from '../../../assets/fundo.png';

export function WrongAnswer(props: any) {

  const abrirTelaTabuada = () => {
    props.navigation.navigate('Tabuada');
  }

  const voltar = () => {
    props.navigation.goBack();
  }

  return (
    <ImageBackground source={errou} style={estilo.fundo} resizeMode="cover">
      <View style={estilo.boxConteudo}>
        <Text style={estilo.texto}>
          Ops! Resposta errada que tal ver a tabuada?
      </Text>

        <View style={estilo.boxBotoes}>
          <View style={estilo.boxBotao}>
            <Button title="Ver tabuada" onPress={abrirTelaTabuada} color="#1f4f66" />
          </View>

          <View style={estilo.boxBotao}>
            <Button title="Tentar outro" onPress={voltar} color="#a0df52" />
          </View>
        </View>
      </View>
    </ImageBackground>
  );
}

const estilo = StyleSheet.create({
  fundo: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    alignItems:"center",
    justifyContent: 'center',
    flex: 1,
  },
  boxConteudo: {
    margin: 20,
    paddingVertical: 20,
    alignItems: "center",
    backgroundColor: 'rgba(255,255,255,0.85)',
    borderRadius: 10,
  },

  texto: {
    marginBottom: 20,
    paddingHorizontal: 10,
    fontSize: 26,
    textTransform: "uppercase",
    fontWeight: "700",
    textAlign: "center",
    color:"#1f4f66"
  },
  boxBotoes: {
    flexDirection: 'row',
  },

  boxBotao: {
    width: 130,
    marginBottom: 10,
    paddingHorizontal: 5
  }
});
