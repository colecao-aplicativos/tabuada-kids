import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import {Home} from '../pages/Home';
import {Table} from '../pages/Table';
import {CorrectAnswer} from '../components/CorrectAnswer';
import {WrongAnswer} from '../components/WrongAnswer';

const Navigation = createStackNavigator();

export function Router() {
  return (
    <NavigationContainer >
      <Navigation.Navigator headerMode='none'>
        <Navigation.Screen name="Home" component={Home} />
        <Navigation.Screen name="Tabuada" component={Table} />
        <Navigation.Screen name="RespostaCorreta"
          component={CorrectAnswer} />
        <Navigation.Screen name="RespostaErrada"
          component={WrongAnswer} />
      </Navigation.Navigator>
    </NavigationContainer>
  );
}
